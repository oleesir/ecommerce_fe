import { Outlet } from 'react-router-dom';
import Header from '../components/Header.tsx';

const MainLayout = () => {
  return (
    <div className='flex w-full'>
      <div className='max-w-5xl  mx-auto '>
        <Header />
        <Outlet />
      </div>
    </div>
  );
};

export default MainLayout;
