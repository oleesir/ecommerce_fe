import {
  createBrowserRouter,
  createRoutesFromElements,
  // Navigate,
  Route,
  RouterProvider,
} from 'react-router-dom';
import Signup from './pages/Signup.tsx';
// import MainLayout from './layouts/MainLayout.tsx';
import AuthLayout from './layouts/AuthLayout.tsx';
import Login from './pages/Login.tsx';
import './App.css';

const router = createBrowserRouter(
  createRoutesFromElements(
    <Route>
      <Route path='/auth' element={<AuthLayout />}>
        <Route path='signup' element={<Signup />} />
        <Route index path='login' element={<Login />} />
      </Route>

      {/*<Route path='/' element={<MainLayout />}>*/}

      {/*</Route>*/}
    </Route>,
  ),
);
function App() {
  return <RouterProvider router={router} />;
}

export default App;
